import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import 'rxjs/add/observable/of';

import { APIService } from '../services/api.service';

@Component({
  selector: 'app-allocation',
  templateUrl: './allocation.component.html',
  styleUrls: ['./allocation.component.scss']
})
export class AllocationComponent {
  @Input() customers: any[];
  source: LocalDataSource; // add a property to the component
  constructor(private _apiService: APIService) {
    this.source = new LocalDataSource(this.customers); // create the source
  }

  ngOnInit() {
    // example apiservice for customers
    this._apiService.getAllAllocations().subscribe(res => {
      if (res.ok) {
        if (res.json().status === 'SUCCESS') {
          this.customers = res.json().data;
          console.log(this.customers);
        }
      }
    }, err => {
      console.log("error occured");
    });
  }

  //settings for the allocation table
  settings = {
    columns: {
      employee_id: {
        title: 'Emp ID',
        editor : {
          type : 'completer',
          config : {
            completer : {
              data : [
                { id: 10, name: 'Nick', email: 'rey@karina.biz' }
              ]
            }
          }
        },
        class : "text-left"
      },
      employee_name: {
        title: 'Emp Name'
      },
      role: {
        title: 'Role'
      },
      location: {
        title: 'Location'
      },
      start_date: {
        title: 'Start Date'
      },
      end_date: {
        title: 'End Date'
      },
      allocation: {
        title: 'Alloc %'
      },
      billing_rate: {
        title: 'Bill Rate'
      },/* 
      billing_currency: {
        title: 'Billing Currency'
      }, */
      amount: {
        title: 'Amount'
      }
    },
    actions : {},
    add : {
      addButtonContent : 'Add Employee',
      confirmCreate : true
    },
    hideSubHeader : true
  };

  //data for the allocation table
  data = [
    {
      "employee_id": "1234",
      "employee_name": "Ashish Santikari",
      "role": "Software Engineer",
      "location": "Bangalore, India",
      "start_date": "2016-12-05",
      "end_date": "2017-06-05",
      "allocation": "20%",
      "billing_rate": 40,
      "billing_currency": "USD",
      "amount": 63360
    },
    {
      "employee_id": "1234",
      "employee_name": "Ashish Santikari",
      "role": "Software Engineer",
      "location": "Bangalore, India",
      "start_date": "2016-12-05",
      "end_date": "2017-06-05",
      "allocation": "20%",
      "billing_rate": 40,
      "billing_currency": "USD",
      "amount": 63360
    },
    {
      "employee_id": "1234",
      "employee_name": "Ashish Santikari",
      "role": "Software Engineer",
      "location": "Bangalore, India",
      "start_date": "2016-12-05",
      "end_date": "2017-06-05",
      "allocation": "20%",
      "billing_rate": 40,
      "billing_currency": "USD",
      "amount": 63360
    },
    {
      "employee_id": "1234",
      "employee_name": "Ashish Santikari",
      "role": "Software Engineer",
      "location": "Bangalore, India",
      "start_date": "2016-12-05",
      "end_date": "2017-06-05",
      "allocation": "20%",
      "billing_rate": 40,
      "billing_currency": "USD",
      "amount": 63360
    },
    {
      "employee_id": "1234",
      "employee_name": "Ashish Santikari",
      "role": "Software Engineer",
      "location": "Bangalore, India",
      "start_date": "2016-12-05",
      "end_date": "2017-06-05",
      "allocation": "20%",
      "billing_rate": 40,
      "billing_currency": "USD",
      "amount": 63360
    }
  ];
}