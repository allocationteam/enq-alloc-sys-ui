import { Component, OnInit, ViewChild } from '@angular/core';
import { SidenavComponent } from '../typescripts/pro/sidenav/sidenav.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @ViewChild('sidenav') sidenavComponent : SidenavComponent;
  constructor(private router : Router) { }

  toggle(){
    this.sidenavComponent.toggle();
  }

  ngOnInit() {

  }

}
