import { FormGroup, FormControl, FormArray, NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form-uploader',
  templateUrl: './form-uploader.component.html',
  styleUrls: ['./form-uploader.component.scss']
})
export class FormUploaderComponent implements OnInit {
  private myForm: FormGroup;
  constructor() { }

  ngOnInit() {
    this.myForm = new FormGroup({
      'employeNumber': new FormControl(),
      'birthYear': new FormControl(),
      'year': new FormControl(),
      'location': new FormGroup({
      'country': new FormControl(),
      'city': new FormControl(),
      'salutation': new FormControl(),
      'firstName': new FormControl(),
      'middleName': new FormControl()
      }),
      'phoneNumbers': new FormArray([new FormControl('')])
    });
  }

  remove(i: number) {
    (<FormArray>this.myForm.get('phoneNumbers')).removeAt(i);
  }

  add() {
    (<FormArray>this.myForm.get('phoneNumbers')).push(new FormControl(''));
  }

 

}
