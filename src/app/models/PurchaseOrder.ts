export interface PurchaseOrder_Raw{
	PurchaseOrderId: number;
	CustomerId     : number;
	PoNum          : string;
	PODescrption   : string;
	Type           : string;
	Amount         : number;
	StartDate      : string;
	EndDate        : string;
	Status         : string;
	PoOwnerId      : number;
	CreatedBy      : string;
	CreatedDate    : string;
	LastUpdated    : string;
	LastUpdatedDate: string;
}

export interface PurchaseOrder{
	purchaseOrderId: number;
	customerId     : number;
	poNum          : string;
	poDescription  : string;
	type           : string;
	amount         : number;
	startDate      : string;
	endDate        : string;
	status         : string;
	poOwnerId      : number;
	createdBy      : string;
	createdDate    : string;
	lastUpdated    : string;
	lastUpdatedDate: string;
}