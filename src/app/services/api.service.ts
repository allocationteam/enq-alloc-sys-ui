import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { PurchaseOrder, PurchaseOrder_Raw } from '../models/PurchaseOrder';

@Injectable()
export class APIService {

  constructor(private _http: Http) { }

  constructUrl(uri: string): string {
    return environment.api_host + uri;
  }

  getAllCustomers(): Observable<any> {
    return this._http.get(this.constructUrl('/api/customers'));
  }
  // irfan change for purchase order
  getAllPurchaseOrder(): Observable<any> {
    return this._http.get(this.constructUrl('/api/purchaseorders'));
  }

  getAllAllocations(): Observable<any> {
    return this._http.get(this.constructUrl('/api/allocations'));
  }

  getPurchaseOrders()  : Observable<PurchaseOrder[]>{
    let url : string = environment.api_host + '/api/purchaseorders';
    return this._http.get(url).map(this.extractPO.bind(this));
  }

  private extractPO(res    : Response) : PurchaseOrder[]{
    let response           = res.json().data.rows;
    return response.map(po => {
      return {
        purchaseOrderId: po.PurchaseOrderId,
        customerId     : po.CustomerId,
        poNum          : po.PoNum,
        poDescription  : po.PODescrption,
        type           : po.Type,
        amount         :po.Amount,
        startDate      : po.StartDate,
        endDate        : po.EndDate,
        status         : po.Status,
        poOwnerId      : po.PoOwnerId
      }
    });
  }



}
