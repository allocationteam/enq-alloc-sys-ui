import { FormGroup, FormControl, FormArray, NgForm } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';

import { APIService } from '../services/api.service';
@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
  @Input() customers: any[];
  private myForm: FormGroup;
  constructor(private _apiService: APIService) {
  }

  ngOnInit() {
    // example apiservice for customers
    this._apiService.getAllPurchaseOrder().subscribe(res => {
      if (res.ok) {
        if (res.json().status === 'SUCCESS') {
          this.customers = res.json().data;
        }
      }
    }, err => {
      console.log("error occured");
    });


    this.myForm = new FormGroup({
      'employeNumber': new FormControl(),
      'birthYear': new FormControl(),
      'location': new FormGroup({
      'country': new FormControl(),
      'city': new FormControl()
      }),
      'phoneNumbers': new FormArray([new FormControl('')])
    });
    //this._apiService.getAllCustomers().subscribe(this.success, this.error);
  }


  // remove(i: number) {
  //   (<FormArray>this.myForm.get('phoneNumbers')).removeAt(i);
  // }

  // add() {
  //   (<FormArray>this.myForm.get('phoneNumbers')).push(new FormControl(''));
  // }
  // success(response) {
  //   console.log(response);
  // }

  // error(){

  // }

}
