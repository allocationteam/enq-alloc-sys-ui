import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate, CanActivateChild } from '@angular/router';
import { AppComponent } from './app.component';

import { EmployeeListComponent } from './employee-list/employee-list.component';
import { PurchaseOrdersComponent } from './purchase-orders/purchase-orders.component';
import { AccountsComponent } from './accounts/accounts.component';
import { MilestoneComponent } from './milestone/milestone.component';
import { CustomersComponent } from './customers/customers.component';
import { VendorsComponent } from './vendors/vendors.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AllocationComponent } from './allocation/allocation.component';
import { FormUploaderComponent} from './form-uploader/form-uploader.component';

const routes: Routes = [
    {
        path : 'dashboard', component : DashboardComponent
    },
    {
        path: 'allocation', component: AllocationComponent
    },
    {
        path: 'form-uploader', component: FormUploaderComponent
    },
    
    {
        path: 'purchase-orders', component: PurchaseOrdersComponent
    },
    {
        path: 'admin', children: [
            {
                path: 'employees', component: EmployeeListComponent
            },
            {
                path: 'customers', component: CustomersComponent
            },
            {
                path: 'vendors', component: VendorsComponent
            },
            {
                path: 'accounts', component: AccountsComponent
            }
        ]
    },
    {
        path : '', pathMatch : 'full' , redirectTo : 'dashboard'
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }