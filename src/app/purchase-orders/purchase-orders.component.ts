import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { APIService } from '../services/api.service';
@Component({
  selector: 'app-purchase-orders',
  templateUrl: './purchase-orders.component.html',
  styleUrls: ['./purchase-orders.component.scss']
})
export class PurchaseOrdersComponent implements OnInit {
  @Input() customers: any[];
  private _isOpen : boolean = false;
  closeAllTips(): void {
    this.customers.forEach((customer) => {
      customer.isOpen = false;
    });
  }

  showContent(customer) {
    if (!customer.isOpen) {
      this.closeAllTips();
    }
    customer.isOpen = !customer.isOpen;
  }
  constructor(private _apiService: APIService) { }
  
  ngOnInit() {
    this._apiService.getAllPurchaseOrder().subscribe(res => {
      if (res.ok) {
        if (res.json().status === 'SUCCESS') {
          this.customers = res.json().data.rows;
        }
      }
    }, err => {
      console.log("error occured");
    });

    //this._apiService.getAllCustomers().subscribe(this.success, this.error);
  }

}
