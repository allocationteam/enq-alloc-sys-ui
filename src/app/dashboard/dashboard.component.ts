import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { MatSort, MatPaginator } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/of';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Http, Response, URLSearchParams } from '@angular/http';
import { PurchaseOrder } from '../models/PurchaseOrder';

import { APIService } from '../services/api.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss'],
	animations: [
		trigger('filterToggle', [
			state('true', style({ display: 'block' })),
			state('false', style({ display: 'none' })),
			transition('true => false', animate('100ms ease-in')),
			transition('false => true', animate('100ms ease-out'))
		])
	]
})
export class DashboardComponent implements OnInit {
	@Input() customers: any[];
	displayFilter: string = 'false';

	toggleFilter(): void {
		this.displayFilter = (this.displayFilter === 'false' ? 'true' : 'false');
	}

	constructor(private _apiService: APIService) {

	}

	dataStore: PurchaseOrder[];

	ngOnInit() {
		this.fetchPurchaseOrders();
		this._apiService.getAllPurchaseOrder().subscribe(res => {
			if (res.ok) {
				if (res.json().status === 'SUCCESS') {
				
					this.customers = res.json().data.rows;
				}
			}
		}, err => {
			console.log("error occured");
		});
	}

	fetchPurchaseOrders(): void {
		this._apiService.getPurchaseOrders().subscribe(res => {
			this.dataStore = res;
		});
	}

}