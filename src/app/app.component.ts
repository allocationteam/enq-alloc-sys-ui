import { Component, ViewChild } from '@angular/core';
import { ToastService } from './typescripts/pro';
import { IMyOptions } from './typescripts/pro/date-picker/index';
import { SidebarComponent } from './sidebar/sidebar.component';

@Component({
  selector: 'mdb-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']

})



export class AppComponent {

  @ViewChild(SidebarComponent) sidebarComponent : SidebarComponent;

  toggleSidebar(){
    this.sidebarComponent.sidenavComponent.toggle();
  }
}
