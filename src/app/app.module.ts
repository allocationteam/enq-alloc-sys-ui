import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
/**/ 
import { ReactiveFormsModule } from '@angular/forms';

// import { ShowErrorsComponent } from './form-uploader/show-errors.component';

// import { BirthYearValidatorDirective } from './form-uploader/validators/birth-year-validators.directive';
// // import { TelephoneNumberFormatValidatorDirective } from './form-uploader/validators/telephone-number-format-validator.directive';
// import { CountryCityValidatorDirective } from './form-uploader/validators/country-city.validators.directive';
// import { TelephoneNumbersValidatorDirective } from './form-uploader/validators/telephone-number.validator.directive';
// import { UniqueNameValidatorDirective } from './form-uploader/validators/name-unique.validators.directive';
// /**/ 
import { HttpModule } from '@angular/http';
import { MDBBootstrapModule } from './typescripts/free';
import { MDBBootstrapModulePro } from './typescripts/pro';
import { AgmCoreModule } from '@agm/core';
import { AppComponent } from './app.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MDBSpinningPreloader } from './typescripts/pro/';
import { MatTableModule, MatSortModule, MatPaginatorModule } from '@angular/material';

import { Ng2SmartTableModule } from 'ng2-smart-table';

import { AppRoutingModule } from './app-routing.module';
import { APIService } from './services/api.service';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { PurchaseOrdersComponent } from './purchase-orders/purchase-orders.component';
import { AccountsComponent } from './accounts/accounts.component';
import { MilestoneComponent } from './milestone/milestone.component';
import { CustomersComponent } from './customers/customers.component';
import { VendorsComponent } from './vendors/vendors.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AllocationComponent } from './allocation/allocation.component';
import { FormUploaderComponent } from './form-uploader/form-uploader.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    EmployeeListComponent,
    PurchaseOrdersComponent,
    AccountsComponent,
    MilestoneComponent,
    CustomersComponent,
    VendorsComponent,
    DashboardComponent,
    AllocationComponent,
    FormUploaderComponent,
    // ShowErrorsComponent,
    // BirthYearValidatorDirective,
    // // TelephoneNumberFormatValidatorDirective,
    // CountryCityValidatorDirective,
    // TelephoneNumbersValidatorDirective,
    // UniqueNameValidatorDirective
  ],
  imports: [
    Ng2SmartTableModule,
    BrowserModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    MDBBootstrapModule.forRoot(),
    MDBBootstrapModulePro.forRoot(),
    NgbModule.forRoot(),
    AgmCoreModule.forRoot({
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en#key
      apiKey: 'Your_api_key'
    }),
    AppRoutingModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule
  ],
  providers: [MDBSpinningPreloader, APIService],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
